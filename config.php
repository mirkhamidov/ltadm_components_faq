<?
$config=array(
  "name" => "Вопросы и ответы",
  "status" => "system",
  "windows" => array(
                      "create" => array("width" => 1000,"height" => 700),
                      "edit" => array("width" => 550,"height" => 400),
                       "list" => array("width" => 550,"height" => 500),
                    ),
  "right" => array("admin","#GRANTED"),
  "main_table" => "lt_faq",
  "list" => array(
    "text" => array("isLink" => true),
    "atext" => array(),
    "is_show" => array("disabledSort" => true),
    "unixtime" => array("align" => "center"),
  ),
  "select" => array(
     "max_perpage" => 20,
     "default_orders" => array(
                           array("unixtime" => "DESC"),
                         ),
  ),
);

?>