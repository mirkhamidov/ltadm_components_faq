<?
$items=array(
  "unixtime" => array(
         "desc" => "Дата размещения",
         "type" => "date",
         "fields" => array(
                      "d" => array("name" => "День","size" => "2", "after" => "."),
                      "m" => array("name" => "Месяц","size" => "2", "after" => "."),
                      "Y" => array("name" => "Год","size" => "4"),
                      "H" => array("name" => "Часы","size" => "2", "after" => ":"),
                      "i" => array("name" => "Минуты","size" => "2"),
                      ),
         "select_on_edit" => true,
         "select_on_edit_disabled" => true,
       ),

  "is_show" => array(
         "desc" => "Показывать на сайте",
         "type" => "radio",
         "values" => array ("yes" => "Да", "no" => "Нет"),
         "default_value" => "yes",
       ),


  "name" => array(
         "desc" => "Имя",
         "type" => "text",
         "maxlength" => "255",
         "size" => "70",
         "select_on_edit" => true,
       ),

  "text" => array(
         "desc" => "Вопрос",
         "type" => "textarea",
         "width" => "120",
         "height" => "10",
         "select_on_edit" => true,
       ),
  "atext" => array(
         "desc" => "Ответ",
         "type" => "editor",
         "width" => "700",
         "height" => "500",
         "select_on_edit" => true,
       ),


);
?>